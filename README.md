# アプリのプロトタイプ

# 試す方法
```bash
git clone -b develop  https://gitlab.com/s1932058jl/demo_app.git

npm install

npm run dev
```

# Problems Page
![Problems Page](https://user-images.githubusercontent.com/102013213/163655018-3da312d4-5e37-4837-bc37-d5f0cc83afc3.png)

# Questions Page

![Questions Page](https://user-images.githubusercontent.com/102013213/163655059-fe26ac29-971e-4074-94b4-47c10c8bd51e.png)

# Single Page for Question
![](https://user-images.githubusercontent.com/102013213/163655177-0443f71a-cdeb-4919-bf2d-37e3b05f0b04.png)
![](https://user-images.githubusercontent.com/102013213/163655147-7a0098b2-d919-4ddf-9e5b-cae47e118b7c.png)
